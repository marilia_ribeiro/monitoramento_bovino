- `python manage.py makemigrations`
- `python manage.py migrate auth`
- `python manage.py migrate`

- `python manage.py populate_app_bovino`

- `sudo su`
- `su postgres`
- `cd app_geografico/management/sqls`
- `psql -U postgres -W -d monitoramento_bovino -f script_insert_app_geografico.sql`

- `cd trajesim/management/sqls`
- `psql -U postgres -W -d monitoramento_bovino -f populate_trajesim.sql`


# Apaga todos os migrations:

- `find . -name "00*" -delete`