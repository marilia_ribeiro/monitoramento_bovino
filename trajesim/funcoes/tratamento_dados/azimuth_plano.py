# -*- coding: utf-8 -*-

from math import *
   
def azimuth_plano(point1,point2):
    lat1,lon1 = radians(point1[0]),radians(point1[1])
    lat2,lon2 = radians(point2[0]),radians(point2[1])
    
    delta_x = projecao(lat2, lat1)
    delta_y = projecao(lon2, lon1)
    
    x = degrees(delta_x)
    y = degrees(delta_y)
    
    if(x >= 0 and y >= 0):
        azimuth = degrees(atan(delta_x/delta_y))
    elif(x >= 0 and y < 0) or (x < 0 and y < 0):
        azimuth = degrees(atan(delta_x/delta_y)) + 180
    elif(x < 0 and y >= 0):
        azimuth = degrees(atan(delta_x/delta_y)) + 360
            
    return azimuth

def projecao(b,a):
    delta = b-a
    return delta;
    
def distance(point1, point2):
    lat1,lon1 = point1[0],point1[1]
    lat2,lon2 = point2[0],point2[1]
    
    delta_x = projecao(lat1, lat2)
    delta_y = projecao(lon1, lon2)
   
    # converte para metros
    x = convert_DD_to_meters(delta_x)
    y = convert_DD_to_meters(delta_y)
    
    return sqrt(x**2 + y**2)
    #return sqrt(delta_x**2 + delta_y**2)

def projecaoDMS(a,b):
    a = float(str.join('', ('', a[0],a[1],a[2])))
    b = float(str.join('', ('', b[0],b[1],b[2])))
    return b - a
        
def distanceDMS(point1, point2):
    lat1,lon1 = point1[0],point1[1]
    lat2,lon2 = point2[0],point2[1]

    #("%.2f" %(float(segundos)))
    
    x = float("%.2f" %(convert_DD_to_meters(abs(float("%.2f" %(projecaoDMS(lat1[:-1], lat2[:-1])))))))
    y = float("%.2f" %(convert_DD_to_meters(abs(float("%.2f" %(projecaoDMS(lon1[:-1], lon2[:-1])))))))
       
    return sqrt(x**2 + y**2)

def convert_DD_to_DMS(point):
    point = str(point).split('.')
    graus = point[0]
    
    point = str(float(str.join('.', ('0', str(int(point[1])))))*60).split('.')
    minutos = point[0]
    
    segundos = str(float(str.join('.', ('0', str(int(point[1])))))*60)
    segundos = ("%.2f" %(float(segundos)))
    
    dms = str.join('', (graus, minutos, segundos))
    return ("%s°%s'%s''"%(graus, minutos, segundos))
   
def DMS_to_DD(p):
    graus = float(p[0])
    minutos = float(p[1])
    segundos = float(p[2])
    sinal = p[3]
        
    segundos = segundos / 60
    minutos = (minutos + segundos)/ 60
    
    dd = graus + minutos
    return signal(sinal, dd)
    
def signal(s,point):
    if(s == 'S' or s == 'W'):
        return str.join('', ('-', str(point)))
    
def convert_DD_to_meters(delta):
    return (delta/60) * 1852
    


def main():
    point1 = [-26.255008, -48.905310]
    point2 = [-26.255535, -48.904979]
    print("az_ab = %s" %(convert_DD_to_DMS(azimuth_plano(point1, point2))))
    print("Postgres: az_ab = %s" %(convert_DD_to_DMS(302.132251530027)))
    print("az_ba = %s" %(convert_DD_to_DMS(azimuth_plano(point2,point1))))
    print("Postgres: az_ba = %s" %(convert_DD_to_DMS(122.132251530027)))
    
    point1 = [-26.2564,-48.90570]
    point2 = [-26.2563, -48.9059]   
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth_plano(point1, point2))))
    print("Postgres: az_ab = %s" %(convert_DD_to_DMS(153.434948822922)))    
    print("az_ba = %s" %(convert_DD_to_DMS(azimuth_plano(point2,point1))))
    print("Postgres: az_ba = %s" %(convert_DD_to_DMS(333.434948822922)))
    
    point1 = [801400, 9836400]
    point2 = [805300, 9839000]   
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth_plano(point1, point2))))
    print("Postgres: az_ab = %s" %(convert_DD_to_DMS(56.3099324740202)))    
    print("az_ba = %s" %(convert_DD_to_DMS(azimuth_plano(point2,point1))))
    print("Postgres: az_ba = %s" %(convert_DD_to_DMS(236.30993247402)))
    
    

        
    return 0

if __name__ == '__main__':
    main()


