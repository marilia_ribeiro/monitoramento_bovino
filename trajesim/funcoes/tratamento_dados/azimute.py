# -*- coding: utf-8 -*-

from math import *

def azimuth(point1,point2):
    """ Formula que calcula o azimute entre dois pontos dispostos no plano terrestre """
    
    lat1,lon1 = point1[0],point1[1]
    lat2,lon2 = point2[0],point2[1]
    
    phi1 = radians(lat1)
    phi2 = radians(lat2)
    delta = radians(lon2 - lon1)
    
    y = sin(delta) * cos(phi2)
    x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(delta)
    
    teta = atan2(x,y)
    
    return (degrees(teta) + 360) % 360

def azimuthes_collect(traject):
    """ Forma uma coleção/lista/array/vetor com Azimutes de uma trajetória especificada """
    
    collection = []
    for i,point in enumerate(traject):
        if i+1 < len(traject): # verifica se há um próximo ponto
            point1 = point              # o ponto atual
            point2 = traject[i+1]  # o ponto seguinte
            az = azimuth(point1,point2) # descobre o azimute entre os dois pontos
            collection.append(az) 
        else:
            collection.append(0)
    return collection

def azimuthes_differences(collection):
    """ Recebendo uma lista de azimutes coletados, a função retorna uma coleção/lista/array/vetor de diferenças entre os azimutes. """
    
    differences = []
    for i,azimuth in enumerate(collection):
        if i+1 < len (collection): # verifica se há um próximo azimute na coleção
            azimuth1 = azimuth          # o azimute atual
            azimuth2 = collection[i+1]  # o azimute seguinte
            difference = abs(azimuth2 - azimuth1) # retira a diferenca
            differences.append(difference)
    return differences

def inspect_traject(traject):
    """ Com um trajeto já em tuplas (coordenadas), a função descobre os azimutes e calcula a diferença destes azimutes do trajeto 
        Repetindo: a função só funciona com trajetorias no formato lista com tuplas: 
        [
            [a,b],
            [c,d],
            [e,f],
            [g,h]
        ]
        
        Caso não esteja no formato sugerido, utilize a classe Format para transformar a linestring em lista.
    """
    collection = azimuthes_collect(traject)
    differences = azimuthes_differences(collection)
    return {'azimutes': collection, 'diferencas': differences}

# FORMATA LINESTRING EM LISTA COM TUPLAS
def format_linestring(linestring):
    """ 
        Formata a linestring, recebendo-a no formatdo do PostGIS 'LINESTRING(1 2, 2 3, 3 4)' e retorna em formato de lista com 
        tuplas de coordenadas -> [[1,2],[2,3],[3,4]] 
    """

    formated_points = []
    
    points_init = linestring.find("(")+1 # Verifica a posição onde iniciam as coordenadas
    traject = linestring[points_init:-1] # delimita a string na posição onde inicia e termina as coordenadas
    for point in traject.split(","): # quebra o trajeto nas coordenadas
        coordinates = point.strip().split(" ") # quebra a coordenada em latitude e longitude
        lat,lon = float(coordinates[0]),float(coordinates[1]) # modifica o tipo para float
        formated_points.append([lat,lon]) # adiciona a latitude e longitude como uma tupla na lista de pontos
    return formated_points

def result_dif_azimutes(linestring):
    return inspect_traject(format_linestring(linestring))
