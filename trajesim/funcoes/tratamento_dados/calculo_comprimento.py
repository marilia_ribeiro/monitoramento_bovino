# -*- coding: utf-8 -*-
from math import *

def calcula_comprimento(coordenadas1, coordenadas2):
    '''Formula de Haversine para medicao de distancias terrestres'''
    lat1,lon1,lat2,lon2 = map(radians, [float(coordenadas1[0]),float(coordenadas1[1]),float(coordenadas2[0]),float(coordenadas2[1])])
    diferenca_lon, diferenca_lat = lon2 - lon1, lat2 - lat1
    a = sin(diferenca_lat/2)**2 + cos(lat1) * cos(lat2) * sin(diferenca_lon/2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    comprimento_km = 6371 * c
    return comprimento_km


def format_linestring(linestring):
    """ 
        Formata a linestring, recebendo-a no formatdo do PostGIS 'LINESTRING(1 2, 2 3, 3 4)' e retorna em formato de lista com 
        tuplas de coordenadas -> [[1,2],[2,3],[3,4]] 
    """
    formated_points = []
    points_init = linestring.find("(")+1 # Verifica a posição onde iniciam as coordenadas
    traject = linestring[points_init:-1] # delimita a string na posição onde inicia e termina as coordenadas
    for point in traject.split(","): # quebra o trajeto nas coordenadas
        coordinates = point.split(" ") # quebra a coordenada em latitude e longitude
        lat,lon = float(coordinates[0]),float(coordinates[1]) # modifica o tipo para float
        formated_points.append([lat,lon]) # adiciona a latitude e longitude como uma tupla na lista de pontos
    return formated_points


def comprimento_total(coordenadas):
    coordenadas = format_linestring(coordenadas)
    comprimento_total = 0.0
    for i in range(len(coordenadas)-1):
        comprimento_total += calcula_comprimento(coordenadas[i],coordenadas[i+1])
    return comprimento_total

