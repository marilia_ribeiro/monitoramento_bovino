# -*- coding: utf-8 -*-

from trajesim.models import SubTrajetoria, Consultas
from trajesim.funcoes.tratamento_dados.tratamento_dados import *
from trajesim.funcoes.tratamento_dados.similaridade_forma import *

def DeterminaPercentualSimilaridade(a,b):
    if a > b:
        a, b = b, a
    return float((a / b) * 100)

def Similaridades(id_consulta,subtrajetoria_referencial):
    trajetorias_similares =[]

    lista_subconsulta = SubTrajetoria.objects.filter(id_consulta=int(id_consulta['id_consulta']))

    referencial =  SubTrajetoria.objects.get(pk=subtrajetoria_referencial)

    tempo_referencial = referencial.sub_tempo
    comprimento_referencial = referencial.sub_comprimento
    referencial_plotavel = ConverteParaPlotagem(str(referencial.sub_trajetoria))

    for objeto in lista_subconsulta:
        if objeto.id_sub_trajetoria != referencial.id_sub_trajetoria:
            similaridade = {}

            similaridade['id_consulta'] = id_consulta['id_consulta']
            similaridade['id_subtrajetoria'] = objeto.id_sub_trajetoria
            similaridade['linestring_plotavel'] = ConverteParaPlotagem(str(objeto.sub_trajetoria).split(";")[1])        
            similaridade['referencial_plotavel'] = ConverteParaPlotagem(str())
            similaridade['similaridade_tempo'] = DeterminaPercentualSimilaridade(objeto.sub_tempo,tempo_referencial)
            similaridade['similaridade_comprimento'] = DeterminaPercentualSimilaridade(objeto.sub_comprimento,comprimento_referencial)
            similaridade['similaridade_forma'] = similaridade_forma(referencial,objeto)

            similaridade['valor_comprimento'] = objeto.sub_comprimento
            similaridade['valor_tempo'] = (objeto.sub_tempo*60)*60

            trajetorias_similares.append(similaridade)

    return trajetorias_similares, referencial_plotavel

def MultiplexadorSimilaridade(similaridades,operador,percentual_tempo,percentual_comprimento,percentual_forma):
    lista_resultado =[]

    contador_pares = 0
    contador_subtrajetorias = 0
    for i,objeto in enumerate(similaridades):
        if float(objeto['similaridade_tempo']) >= int(percentual_tempo) and \
            float(objeto['similaridade_comprimento']) >= int(percentual_comprimento):
            
            trechos = []

            for forma in objeto['similaridade_forma']:
                if float(forma['similaridade_forma']) >= int(percentual_forma):
                    par_plotavel = AgrupaTrajetos(forma['trajetos'])
                    trechos.append({
                        'contador': contador_pares,
                        'id': forma['id'], 
                        'par_plotavel': par_plotavel, 
                        'similaridade': forma['similaridade_forma']
                    })

                    contador_pares += 1

            if(len(trechos)>0):
                objeto['contador'] = contador_subtrajetorias
                objeto['similaridade_forma'] = trechos
                lista_resultado.append(objeto)

                contador_subtrajetorias += 1
                     
    return lista_resultado

def VerificaSimilaridade(percentual_tempo,percentual_comprimento,id_consulta,subtrajetoria_referencial,operador,percentual_forma):
    similaridades, trajref_plotavel = Similaridades(id_consulta,subtrajetoria_referencial)
    resultado_similaridade = MultiplexadorSimilaridade(similaridades,operador,percentual_tempo,percentual_comprimento,percentual_forma)
    
    if resultado_similaridade:
        return resultado_similaridade, trajref_plotavel
    return 0, 0


