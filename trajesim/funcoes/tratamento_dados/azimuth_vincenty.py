# -*- coding: utf-8 -*-

from math import *

# https://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
# https://en.wikipedia.org/wiki/Vincenty%27s_formulae

# validar cálculo (Transporte de Coordenadas -> Transporte de Coordenadas - Cálculos -> Método Inverso ->Coordenadas Geodésicas):
# http://www.ufrgs.br/engcart/Teste/inicial.html

def azimuth(point1,point2):
    """ Formula de Vincenty - método inverso para o calculo do azimute
    entre dois pontos dispostos no plano terrestre
    Onde:
    a =
    b =
    f =
    U1 e U2 =
    L =
    Lambda =
    azimuth =
    """

    latitude1,longitude1 = point1[0], point1[1]
    latitude2,longitude2 = point2[0], point2[1]

    #retorna 0 se os pontos forem iguais ou quase iguais
    if (abs(latitude2 - latitude1) < 1e-8) and (abs(longitude2 - longitude1) < 1e-8):
        #return 0.0
        return 0.0, 0.0

    latitude1,longitude1 = radians(point1[0]), radians(point1[1])
    latitude2,longitude2 = radians(point2[0]), radians(point2[1])

    #a = 6378137.0 # metres
    #f = (a - b)/a
    f = (1/298.257223563) #achatamento da terra pelo WGS84
    U1 = atan((1 - f) * tan(latitude1))
    U2 = atan((1 - f) * tan(latitude2))

    L = longitude2 - longitude1
    Lambda = L #primeira aproximação

    sinU1 = sin(U1)
    sinU2 = sin(U2)
    #sinLambda = sin(Lambda)

    cosU1 = cos(U1)
    cosU2 = cos(U2)
    #cosLambda = cos(Lambda)

    max_iterations = 1000
    for iteration in range(max_iterations):
        sinSigma = sqrt(pow(cosU2 * sin(Lambda),2) + pow(cosU1 * sinU2 - sinU1 * cosU2 * cos(Lambda),2))
        cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cos(Lambda)
        sigma = atan(sinSigma/cosSigma)

        sinAlpha = (cosU1 * cosU2 * sin(Lambda))/sinSigma
        alpha = asin(sinAlpha)
        cosAlpha = cos(alpha)
        #cos2Alpha = pow(1 - pow(sinAlpha,2),2)
        cos2Alpha = pow(cosAlpha,2)
        cos2SigmaM = cosSigma - ((2 * sinU1 * sinU2)/cos2Alpha)

        C = (f/16) * cos2Alpha * (4 + f * (4 - 3 * cos2Alpha))

        Lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * pow(cos2SigmaM, 2))))


    # ----- Azimute Horário ----- #
    x12 = cosU2 * sin(Lambda)
    y12 = cosU1 * sinU2 - sinU1 * cosU2 * cos(Lambda)
    azimuth12 = atan(x12/y12)

    # ----- Azimute Antihorário ----- #
    x21 = cosU1 * sin(Lambda)
    y21 = -sinU1 * cosU2 + cosU1 * sinU2 * cos(Lambda)
    azimuth21 = atan(x21/y21)

    two_pi = 2 * pi

    # ----- Azimute Horário ----- #
    if ( azimuth12 < 0.0 ) :
            azimuth12 =  azimuth12 + two_pi
    if ( azimuth12 > two_pi ) :
            azimuth12 = azimuth12 - two_pi

    # ----- Azimute Antihorário ----- #
    azimuth21 = azimuth21 + two_pi / 2.0
    if (azimuth21 < 0.0):
        azimuth21 = azimuth21 + two_pi
    if (azimuth21 > two_pi):
        azimuth21 = azimuth21 - two_pi


    #azimuth = degrees(azimuth12)
    #return azimuth
    return [degrees(azimuth12), degrees(azimuth21)]

def convert_DD_to_DMS(point):
    point = str(point).split('.')
    graus = point[0]

    point = str(float(str.join('.', ('0', str(int(point[1])))))*60).split('.')
    minutos = point[0]

    segundos = str(float(str.join('.', ('0', str(int(point[1])))))*60)
    segundos = ("%.2f" %(float(segundos)))

    dms = str.join('', (graus, minutos, segundos))
    return ("%s°%s'%s''"%(graus, minutos, segundos))

def projecaoDMS(a,b):
    a = float(str.join('', ('', a[0],a[1],a[2])))
    b = float(str.join('', ('', b[0],b[1],b[2])))
    return b - a

def main():
    # Forward Azimuth
    # 306°52'05.37" ou 306.868159
    #
    # Reverse Azimuth
    # 127°10'25.07" ou 127.173631
    point1 = [-37.95103341666667, 144.42486788888888]
    point2 = [-37.65282113888889, 143.92649552777777]
    print("az_ab    Esperado: 306°52'05.37''     Obtido: %s" %(convert_DD_to_DMS(azimuth(point1, point2)[0])))
    print("az_ba    Esperado: 127°10'25.07''     Obtido: %s" % (convert_DD_to_DMS(azimuth(point1, point2)[1])))

    point1 = [0.066593, 0.998996]
    point2 = [0.214407, 0.998996]
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth(point1, point2)[0])))
    print("\naz_ba = %s" %(convert_DD_to_DMS(azimuth(point1, point2)[1])))

    '''point1 = [0.066593, 0.998996]
    point2 = [0.214407, -1.366025]
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth(point1, point2))))

    point1 = [-26.255008, -48.905310]
    point2 = [-26.255535, -48.904979]
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth(point1, point2))))

    point1 = [-26.2564,-48.90570]
    point2 = [-26.2563, -48.9059]
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth(point1, point2))))

    point1 = [801400, 9836400]
    point2 = [805300, 9839000]
    print("\naz_ab = %s" %(convert_DD_to_DMS(azimuth(point1, point2))))
    '''
    
    
    
    return 0

if __name__ == '__main__':
    main()
