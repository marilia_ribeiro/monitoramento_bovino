# -*- coding: utf-8 -*-
from math import *
from trajesim.funcoes.tratamento_dados.tratamento_dados import *

from trajesim.models import *

def IdentificaCentroZoom(poligono):
    """
        Recebe o poligono e retorna o ponto central e o zoom recomendável ao mesmo.
    """
    lat = []
    lon = []
    for ponto in poligono.split():
        coordenadas = ponto.split(",")
        lat.append(float(coordenadas[0]))
        lon.append(float(coordenadas[1]))
    centro = IdentificaCentro(lat,lon)
    zoom = DeterminaZoom(lat,lon)    
    return centro,zoom

def IdentificaCentro(latitudes,longitudes):
    """
        Recebe uma lista de latitudes e uma lista de longitudes e através
        dos valores máximos e mínimos retorna a latitude média e a longitude média,
        ou seja, o ponto central do polígono.

        >>> Centro([0,3,6,10],[5,6,9,15])
        "5,10"
    """
    latitude_central = (max(latitudes)+min(latitudes))/2
    longitude_central = (max(longitudes)+min(longitudes))/2
    return "%s,%s" %(latitude_central,longitude_central)

def DeterminaZoom(latitudes,longitudes):
    """
        Recebe uma lista de latitudes e uma lista de longitudes e retorna
        o zoom máximo necessário para que todos os pontos possam ser visualidos sem alteração
        manual da aproximação.

        >>> Zoom([-33,-10,0,55,120],[-62,-15,10,37,68])
        2
    """
    distancia = IdentificaMaiorDistancia(latitudes,longitudes)
    
    limitesDeZoom = [180,150,120,90,60,15,7.5,3.75,1.875,0.9375,0.46875,0.234375,0.1171875,0.05859375,0.029296875,0]
    for zoom,l in enumerate(limitesDeZoom):
        if distancia > limitesDeZoom[zoom+1]:
            return zoom

def IdentificaMaiorDistancia(latitudes,longitudes):
    """
        Recebe uma lista de latitudes e uma lista de longitudes e utilizando
        valores máximos/mínimos e cálculo de distância entre pontos, retorna
        a maior distancia considerando os extremos do polígono.

        >>> DefineMaiorDistancia([0,6,10,15],[-5,2,7,20])
        -25
    """
    dLatitudes = sqrt((max(latitudes) - min(latitudes))**2)
    dLongitudes = sqrt((max(longitudes) - min(longitudes))**2)
    
    if dLatitudes >= dLongitudes:
        return dLatitudes
    else:
        return dLongitudes

def IdentificaPontosIniciais():
    """
        Consulta as trajetorias registradas no banco com o intuito
        relacionar apenas o primeiro ponto de cada trajetória.
        Retorna uma lista com esses pontos.
    """

    busca = Trajetorias.objects.only("trajetoria").all()
    
    pontos_iniciais = []
    for row in busca:
        trajetoria = ConverteParaPlotagem(str(row.trajetoria))
        pontos = trajetoria.split(" ")
        pontos_iniciais.append(pontos[0])

    return pontos_iniciais