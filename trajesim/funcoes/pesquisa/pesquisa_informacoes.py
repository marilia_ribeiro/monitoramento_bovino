# -*- coding: utf-8 -*-

from trajesim.models import Consultas, Pontos, Trajetorias, SubTrajetoria
from trajesim.funcoes.tratamento_dados.tratamento_dados import *
from datetime import datetime
from django.db.models import Max,Min,Count

from monitoramento_bovino.settings import *

def PesquisaPeriodo(data_inicio, data_fim):
    '''
    Recebe um periodo de pesquisa e verifica se existem trajetorias nele, se existir algum 
    registro, retorna o id da trajetoria e a propria trajetoria em LineString "pronta para ser plotada
    pelo navegador", pois ela já passou pela função ConverteParaPlotagem.

    >>> PesquisaPeriodo('1/05/2000','20/03/2002')
    []
    '''

    id_trajetorias = VerificaPeriodo(data_inicio, data_fim)
    resultado_tejatorias = []

    for id_trajetoria in id_trajetorias:
        dados_trajetoria = BuscaLineString(id_trajetoria)
        dados_pontos_trajetoria = getInformacoesPontos(id_trajetoria)



        trajetoria = {}
        trajetoria['id_trajetoria'] = id_trajetoria
        # trajetoria['trajetoria'] = ConverteParaPlotagem(str(BuscaLineString(id_trajetoria)))
        trajetoria['trajetoria'] = ConverteParaPlotagem(str(dados_trajetoria.trajetoria))
        trajetoria['descricao'] = dados_trajetoria.descricao
        trajetoria['data_inicio'] = dados_trajetoria.data_hora_inicio
        trajetoria['data_fim'] = dados_trajetoria.data_hora_fim
        trajetoria['data_fim'] = dados_trajetoria.data_hora_fim
        trajetoria['tempo'] = dados_trajetoria.tempo
        trajetoria['comprimento'] = dados_trajetoria.comprimento
        trajetoria['velocidade_media'] = dados_trajetoria.velocidade_media
        trajetoria['velocidade_minima'] = dados_pontos_trajetoria[0]
        trajetoria['velocidade_maxima'] = dados_pontos_trajetoria[1]
        trajetoria['nome_animal'] = dados_trajetoria.bovino.nome
        trajetoria['brinco'] = dados_trajetoria.bovino.brinco
        trajetoria['quantidade_pontos'] = dados_pontos_trajetoria[2]
        trajetoria['primeiro_ponto'] = dados_pontos_trajetoria[3].ponto
        resultado_tejatorias.append(trajetoria)

    return resultado_tejatorias

def getInformacoesPontos(id_trajetoria):
    velocidade_minima = Pontos.objects.filter(id_trajetoria=id_trajetoria).aggregate(Min('velocidade'))
    velocidade_maxima = Pontos.objects.filter(id_trajetoria=id_trajetoria).aggregate(Max('velocidade'))
    quantidade_pontos = Pontos.objects.filter(id_trajetoria=id_trajetoria).aggregate(Count('id_ponto'))
    primeiro_ponto = Pontos.objects.filter(id_trajetoria=id_trajetoria)[0]

    return (velocidade_minima, velocidade_maxima, quantidade_pontos,primeiro_ponto)


def VerificaPeriodo(data_inicio, data_fim):

    ''' 
    Função recebe uma data inicial e final, converte as datas inicial e final para um 
    formato que a possibilite a consulta no banco e retorna todas as trajetorias que 
    estão no periodo selecionado e retorna uma lista com IDs das trajetorias.

    >>> VerificaPeriodo('1/1/2000','20/03/2015')
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37]

    >>> VerificaPeriodo('1/05/2014','20/03/2015')
    [34, 35, 36, 37]
    '''
    resultado_tejatorias = []

    # data_inicio = AjustaData(data_inicio)
    # data_fim = AjustaData(data_fim)
    #
    # data_inicio = datetime.strptime('%s' %(data_inicio), '%d-%m-%Y').date()
    # data_fim = datetime.strptime('%s' %(data_fim), '%d-%m-%Y').date()
    resultado = Pontos.objects.filter(data_hora_ponto__range=(data_inicio,data_fim))

    for linha in resultado:
        if linha.id_trajetoria_id not in resultado_tejatorias:
            resultado_tejatorias.append(linha.id_trajetoria_id)

    return resultado_tejatorias


def BuscaLineString(id_linestring):
    '''
    Recebe o id da linestring, pesquisa no banco o objeto correspondente ao id informado e 
    retorna o a LINESTRING no formato passado pelo Banco.

    >>>BuscaLineString(1)
    LINESTRING (23.9362299999999983 37.9603230000000025, 23.9471209999999992 37.9595640000000003,
     23.9491809999999994 37.9591250000000002, 23.9527110000000008 37.9587429999999983)
    '''
    linestring = Trajetorias.objects.get(id_trajetoria = '%s' %(id_linestring))
    # return linestring.trajetoria
    return linestring


def VerificaInterseccao(poligono):
    try:
        consulta = Trajetorias.objects.raw("SELECT id_trajetoria, st_astext(st_intersection(st_geometryfromtext('%s',4326),trajetoria)) as trajeto FROM %s WHERE st_intersects(st_geometryfromtext('%s',4326),trajetoria)" %(poligono,TABLE_TRAJETORIAS, poligono))
        trajetos_separados_em_mult_line = []
        for linha in consulta:
            informacoes_trajetoria = {}
            informacoes_trajetoria['id'] = linha.id_trajetoria
            informacoes_trajetoria['trajetoria'] = linha.trajeto
            trajetos_separados_em_mult_line.append(informacoes_trajetoria)

        return trajetos_separados_em_mult_line
    
    except:
        return False

def ProcessaConsulta(poligono):
    trajetorias_em_line_e_mult = VerificaInterseccao(poligono)

    if trajetorias_em_line_e_mult:
        trajetorias_em_lines = SeparaTrajetorias(trajetorias_em_line_e_mult)
        trajetos_plotar =  AjustaPlotagem(trajetorias_em_lines)

        return trajetos_plotar, trajetorias_em_lines
    else:
        return False

def BuscaSubTrajetorias(consulta):
    subtrajetorias = []
    consulta = SubTrajetoria.objects.raw("SELECT id_sub_trajetoria, st_astext(sub_trajetoria) as subtrajeto FROM %s WHERE id_consulta_id = %i" %(TABLE_SUBTRAJETORIA,consulta))
    #consulta = SubTrajetoria.objects.raw("SELECT id_sub_trajetoria, st_astext(sub_trajetoria) as subtrajeto FROM trajv2_subtrajetoria WHERE id_consulta_id = %i" %consulta)

    for linha in consulta:
        subtrajeto_plotavel = ConverteParaPlotagem(linha.subtrajeto)
        informacoes_subtrajetoria = {'id': linha.id_sub_trajetoria, 'subtrajeto': subtrajeto_plotavel}
        subtrajetorias.append(informacoes_subtrajetoria)
    
    return subtrajetorias

def ReferencialDetalhes(id):
    objeto = SubTrajetoria.objects.get(pk=id)
    referencial = {
        'valor_comprimento': objeto.sub_comprimento,
        'valor_tempo': (objeto.sub_tempo*60)*60
    }
    return referencial


def getInformacoesTrajetoria(trajetoria):
    trajetoria = ConverteParaLinestring(trajetoria, 'linestring')

    # query = "SELECT * FROM %s WHERE st_equals(%s, st_astext(%s))" % (TABLE_TRAJETORIAS, trajetoria, 'trajetoria')
    consulta = Trajetorias.objects.raw("SELECT * FROM %s WHERE st_equals(%s, st_astext(%s))", [TABLE_TRAJETORIAS, trajetoria, 'trajetoria'])
    print (consulta)

    return consulta