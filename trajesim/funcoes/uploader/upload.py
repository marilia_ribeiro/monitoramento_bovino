# -*- coding: utf-8 -*-
from trajesim.models import Upload

def processaUpload(file):
	validacao = validaArquivo(file)
	if validacao == True:
		upload = Upload()
		upload.titulo = file.name
		upload.arquivo = file
		upload.save()
		return True
	else:
		return validacao

def validaArquivo(file):
	# Para o arquivo ser correto, deve haver pelo menos um ponto válido (com 7 campos)
	# e que cada linha válida tenha seus campos nos formatos corretos

	linhas_validas = []
	
	if verificaDuplicidade(file):
		if verificaFormato(file.name): # se o formato for válido
			for linha in file.readlines():
				linha = linha.rstrip().decode('utf-8')
				dados = linha.split(",")
				
				if len(dados) == 7 and linha != '0,2,255,My Track,0,0,2,8421376': # se existem o num. de dados e não for um exemplo
					
					data,hora = dados[5],dados[6]
					if data.count("-") != 2 or hora.count(":") != 2: # se a data e a hora forem validas
						return "O Formato de data é incorreto. Deverá ser AAAA-MM-DD!"
					linhas_validas.append(linha)

			if len(linhas_validas) > 0: # se existem linhas válidas
				return True
			else:
				return "Não existem pontos válido nesse arquivo"
		else:
			return "Formato de arquivo incorreto"
	else:
		return "Este arquivo já está no sistema e aguarda o processamento"

def verificaFormato(file):
	formatos_validos = "txt plt"
	extensao_do_arquivo = file.split(".")[-1:][0]

	for formato in formatos_validos.split():
		if formato == extensao_do_arquivo:
			return True
	return False

def verificaDuplicidade(file):
	consulta = Upload.objects.filter(titulo=file.name)
	if len(consulta) == 0:
		return True
	else:
		return False