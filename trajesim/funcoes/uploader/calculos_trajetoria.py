#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  calculos_trajetoria.py
#

import datetime
import itertools
from math import *

#-------------------------------------------------------------------------------------------------------#
#---------------------------------------------CALCULO TEMPO---------------------------------------------#
#-------------------------------------------------------------------------------------------------------#
    
def determina_tempo_decorrido(tempos):
    #Recebe uma lista de momentos de tempo e devolve o tempo decorrido entre eles
    if len(tempos) == 1:
        return 0
    tempos.sort()
    diferenca_tempo = abs(tempos[0] - tempos[-1])
    tempo_em_segundos = diferenca_tempo.total_seconds()
    return tempo_em_segundos
    
def calculo_tempo(tempo_em_segundos):
    tempo_em_segundos = int(tempo_em_segundos)
    minu = 0
    hor = 0
    while True:
        if tempo_em_segundos > 60:
            minu += 1
            tempo_em_segundos -= 60
        elif minu > 60:
            hor += 1
            minu -= 60
        else: break
    return (hor, minu, tempo_em_segundos)

#-------------------------------------------------------------------------------------------------------#
#----------------------------------------CALCULO COMPRIMENTO--------------------------------------------#
#-------------------------------------------------------------------------------------------------------#

def separa_latitude_longitude(trajetorias):
    coordenadas_trajetorias = {}
    coordenada = []
    pontos = []
    for trajetoria in trajetorias:
        latitude,longitude = trajetoria.split("  ")
        pontos.append(latitude)
        pontos.append(longitude)
        coordenada.append(pontos)
        pontos = []
    coordenadas_trajetorias[1] = coordenada
    return coordenadas_trajetorias
    
def separa_e_combina_pontos_por_trajetoria(coordenadas_trajetoria):
    pontos_trajetorias = {}
    for chave in coordenadas_trajetoria.keys():
        if len(coordenadas_trajetoria.get(chave)) > 1:
            pontos_trajetorias.update({chave:combina_pontos_de_dois_em_dois(coordenadas_trajetoria.get(chave))})
        else: pontos_trajetorias.update({chave:[0]})
    return pontos_trajetorias

def combina_pontos_de_dois_em_dois(pontos):
    combinacao = []
    x = 0
    try:
        for ponto in pontos:
            combinacao.append((pontos[x][0],pontos[x][1],pontos[x+1][0],pontos[x+1][1]))
            x += 1
    except: return combinacao
    
def determina_comprimento_total(combinacao):
    comprimento_trajetorias = {}
    for chave in combinacao.keys():
        comprimento = 0
        for pontos in combinacao[chave]: comprimento += calcula_comprimento((pontos[0], pontos[1], pontos[2], pontos[3]))
        comprimento_trajetorias.update({chave:comprimento})
    return comprimento_trajetorias[1]
    
def calcula_comprimento(coordenadas):
    '''Formula de Haversine para medicao de distancias terrestres'''
    lat1,lon1,lat2,lon2 = map(radians, [float(coordenadas[0]),float(coordenadas[1]),float(coordenadas[2]),float(coordenadas[3])])
    diferenca_lon, diferenca_lat = lon2 - lon1, lat2 - lat1
    a = sin(diferenca_lat/2)**2 + cos(lat1) * cos(lat2) * sin(diferenca_lon/2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    comprimento_km = 6371 * c
    return comprimento_km
    
#-------------------------------------------------------------------------------------------------------#
#----------------------------------------CALCULO VELOCIDADE MÉDIA---------------------------------------#
#-------------------------------------------------------------------------------------------------------#
    
def calcula_velocidade_media_trajetoria(tempo_seg, comprimento_km):
	return (comprimento_km / ((tempo_seg/60)/60))
