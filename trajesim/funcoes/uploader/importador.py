# -*- coding: utf-8 -*-

import psycopg2
import datetime
from trajesim.funcoes.uploader.calculos_trajetoria import *
from monitoramento_bovino.settings import *
#con = psycopg2.connect("dbname='"+ DB_NAME + "' user='"+ DB_USER +"' password='"+ DB_PASSWORD +"' host='"+ DB_HOST +"'")
con = psycopg2.connect("dbname='%s' user='%s' password='%s' host='%s'" %(DB_NAME,DB_USER,DB_PASSWORD,DB_HOST))
#con = psycopg2.connect("dbname='trajesim' user='postgres' password='postgres' host='localhost'")
cur = con.cursor()

def agrupa_data_hora(data,hora):
    ano,mes,dia = data.split("-")
    hora,minuto,segundo = hora.split(":")
    
    data_hora = datetime.datetime(
        int(ano),
        int(mes),
        int(dia),
        int(hora),
        int(minuto),
        int(segundo)
    )

    return data_hora

def captura_dados(arquivo):
    arquivo = open(arquivo,'r')

    pontos = []
    for linha in arquivo.readlines():
        dados = linha.split(",")
        if len(dados) == 7:
            ponto = {
                'latitude': float(dados[0]),
                'longitude': float(dados[1]),
                'data-hora': agrupa_data_hora(dados[5],dados[6].replace("\n",""))
            }
            pontos.append(ponto)
    return pontos

def insere_pontos(dados):
    #cons = cur.execute("SELECT max(id_trajetoria) FROM " + TABLE_TRAJETORIAS)
    cons = cur.execute("SELECT max(id_trajetoria) FROM %s" %(TABLE_TRAJETORIAS))
    #cons = cur.execute("SELECT max(id_trajetoria) FROM trajv2_trajetorias")
    id_atual = cur.fetchall()[0][0]

    for ponto in dados:
        coordenada = "%s %s" %(ponto['latitude'],ponto['longitude'])
        data = ponto['data-hora']
        #query = "INSERT INTO " + TABLE_PONTOS + "(id_trajetoria_id,data_hora_ponto,ponto) VALUES (%i,'%s',st_geometryfromtext('POINT(%s)',4326))" %(id_atual,data,coordenada)
        query = "INSERT INTO %s(id_trajetoria_id,data_hora_ponto,ponto) VALUES (%i,'%s',st_geometryfromtext('POINT(%s)',4326))" %(TABLE_PONTOS,id_atual,data,coordenada)
        # query = "INSERT INTO trajv2_pontos(id_trajetoria_id,data_hora_ponto,ponto) VALUES (%i,'%s',st_geometryfromtext('POINT(%s)',4326))" %(id_atual,data,coordenada)
        insere(query)
    return 0

def prepara_linestring(coordenadas,tempo,comprimento,velocidade_media):
    #return "INSERT INTO " + TABLE_TRAJETORIAS + "(trajetoria,tempo,comprimento,velocidade_media) VALUES(st_geometryfromtext('LINESTRING(%s)',4326),%i,%f,%f)" % (",".join(coordenadas), tempo, comprimento, velocidade_media)
    return "INSERT INTO %s(trajetoria,tempo,comprimento,velocidade_media) VALUES(st_geometryfromtext('LINESTRING(%s)',4326),%i,%f,%f)" %(TABLE_TRAJETORIAS,",".join(coordenadas),tempo,comprimento,velocidade_media)
    #return "INSERT INTO trajv2_trajetorias(trajetoria,tempo,comprimento,velocidade_media) VALUES(st_geometryfromtext('LINESTRING(%s)',4326),%i,%f,%f)" %(",".join(coordenadas),tempo,comprimento,velocidade_media)

def insere(query):
    try:
        cur.execute(query)
        con.commit()
        return True
    except psycopg2.Error as e:
        print(e.pgerror)
        pass

def prepara_dados_para_registro(dados):
    tempos,pontos,coordenadas,pontos_combinados = [],[],[],[]
    
    for i,ponto in enumerate(dados):
        coordenada = "%s %s" %(ponto['latitude'],ponto['longitude'])

        coordenadas.append(coordenada)
        tempos.append(ponto['data-hora'])
        
        if i+1<len(dados):
            ponto_seguinte = dados[i+1]
            pontos_combinados.append([
                ponto['latitude'],
                ponto['longitude'],
                ponto_seguinte['latitude'],
                ponto_seguinte['longitude']
            ])
    
    comprimento = determina_comprimento_total({1:pontos_combinados})
    tempo = determina_tempo_decorrido(tempos)
    velocidade_media = calcula_velocidade_media_trajetoria(tempo,comprimento)
    linestring = prepara_linestring(coordenadas,tempo,comprimento,velocidade_media)
    
    insere(linestring)
    insere_pontos(dados)
    return 0

def importa_dados():
    arquivos = os.listdir(MEDIA_UPLOADER)
    
    try:
        for i,arquivo in enumerate(arquivos):
            dados = captura_dados(MEDIA_UPLOADER+arquivo)
            prepara_dados_para_registro(dados)
        return True
    except:
        return False
