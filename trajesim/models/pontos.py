#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.gis.db import models
from trajesim.models.trajetorias import Trajetorias

class Pontos(models.Model):
    ponto = models.PointField()
    id_trajetoria = models.ForeignKey(Trajetorias)
    data_hora_ponto = models.DateTimeField(blank=False, help_text='Data e hora da coleta:')
    acelerometro_x = models.FloatField()
    acelerometro_y = models.FloatField()
    acelerometro_z = models.FloatField()
    velocidade = models.FloatField()

    id_ponto = models.AutoField(primary_key=True)
    # id_trajetoria = models.ForeignKey(Trajetorias)
    # data_hora_ponto = models.DateTimeField()
    # ponto = models.PointField()

    objects = models.GeoManager()
