﻿alter table trajesim_trajetorias add column trajetoria_yx geometry(LINESTRING,4326);
update trajesim_trajetorias set trajetoria_yx=trajetoria;

alter table trajesim_trajetorias add column trajetoria_xy geometry(LINESTRING,4326);
update trajesim_trajetorias set trajetoria_xy=ST_FlipCoordinates(trajetoria);

update trajesim_trajetorias set trajetoria=trajetoria_xy;

--select st_astext(trajetoria), st_astext(trajetoria_yx), st_astext(trajetoria_xy) from trajesim_trajetorias;

--st_astext(ST_FlipCoordinates(trajetoria))

--ALTER TABLE sometable ALTER COLUMN geom TYPE geometry(LineString,4326) USING ST_FlipCoordinates(geom)::geometry(LineString,4326);

--ALTER TABLE trajesim_trajetorias ALTER COLUMN trajetoria_yx TYPE geometry(LineString,4326) USING ST_FlipCoordinates(trajetoria)::geometry(LineString,4326);
