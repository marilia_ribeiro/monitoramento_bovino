alter table trajesim_pontos add column ponto_yx geometry(POINT,4326);
update trajesim_pontos set ponto_yx=ponto;

alter table trajesim_pontos add column ponto_xy geometry(POINT,4326);
update trajesim_pontos set ponto_xy=ST_FlipCoordinates(ponto);

update trajesim_pontos set ponto=ponto_xy;
