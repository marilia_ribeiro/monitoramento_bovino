﻿UPDATE trajesim_trajetorias SET tempo=EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio));

UPDATE trajesim_trajetorias SET comprimento=st_length(trajetoria)/1000;

UPDATE trajesim_trajetorias SET velocidade_media=comprimento/((tempo/60)/60);