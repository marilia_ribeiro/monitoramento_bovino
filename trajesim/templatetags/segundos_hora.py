from django import template
register = template.Library()

@register.filter(name='segundoshoras')
def segundoshoras(value):
    segundos = int(value)

    dias = segundos // 86400
    segundos_rest = segundos % 86400
    horas = segundos_rest // 3600
    segundos_rest = segundos_rest % 3600
    minutos = segundos_rest // 60
    segundos_rest = segundos_rest % 60

    return('%s dias %s:%s:%s' %(dias,horas,minutos,segundos_rest))