from django import template
from django.db import connection
from monitoramento_bovino.settings import TABLE_TRAJETORIAS
from trajesim.funcoes.pesquisa.pesquisa_informacoes import getInformacoesTrajetoria
from trajesim.funcoes.tratamento_dados.tratamento_dados import ConverteParaConsulta, ConverteParaLinestring
from trajesim.models import Trajetorias, Pontos

register = template.Library()

@register.filter(name='informacoes_trajetoria')
def informacoes_trajetoria(value):
    # return getInformacoesTrajetoria(value)

    trajetoria = ConverteParaLinestring(value, 'linestring')

    # query = "SELECT * FROM %s WHERE st_equals(%s, st_astext(%s))" % (TABLE_TRAJETORIAS, trajetoria, 'trajetoria')
    consulta = Trajetorias.objects.raw("SELECT * FROM %s WHERE st_equals(%s, st_astext(%s))", [TABLE_TRAJETORIAS, trajetoria, 'trajetoria'])

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM %s WHERE st_equals(%s, st_astext(%s))" % (TABLE_TRAJETORIAS, trajetoria, 'trajetoria'))
        row = cursor.fetchone()
        print(row)
        return row