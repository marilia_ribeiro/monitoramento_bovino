# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from trajesim.funcoes.auxiliares_web.auxiliares_JavaScript import *
from trajesim.funcoes.pesquisa.pesquisa_informacoes import *

# Criação do Poligono
class Passo2Views(View):
    # ex passo3

    def get(self, request):
        if 'id_consulta' in request.session:
            try:
                del request.session['id_consulta']
                
                periodo = request.session['periodo']
                resultado_periodo = PesquisaPeriodo(periodo['data_inicial'],periodo['data_final'])

                return render(request, 'trajesim/passo2.html', {
                        'passo': 2,
                        'trajetos': resultado_periodo
                    })
            except:
                return HttpResponseRedirect('/trajetorias/1/')
        else:
            return HttpResponseRedirect('/trajetorias/1/')

    def post(self, request):
        
        periodo = {
            'data_inicial': request.POST.get('data_inicio'), 
            'data_final': request.POST.get('data_fim')
        }
        request.session['periodo'] = periodo
        resultado_periodo = PesquisaPeriodo(periodo['data_inicial'],periodo['data_final'])

        return render(request, 'trajesim/passo2.html', {
                'passo': 2,
                'trajetos': resultado_periodo
            })
