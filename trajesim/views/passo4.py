# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from trajesim.funcoes.tratamento_dados.tratamento_dados import *
from trajesim.funcoes.auxiliares_web.auxiliares_JavaScript import *
from trajesim.funcoes.persistencia_banco.salva_consulta import *
from trajesim.funcoes.persistencia_banco.salva_subtrajetorias import *
from trajesim.funcoes.pesquisa.pesquisa_informacoes import *


class Passo4Views(View):
    def get(self, request):
        if 'globais' in request.session and 'subtrajetoria_referencial' in request.session:
            return render(request, 'trajesim/passo4.html', {
                    'passo': 4,
                    'global': request.session['globais']
                })
        else:
            return HttpResponseRedirect('/trajetorias/1/')

    def post(self, request):
    
        id_referencial = request.POST.get('trajetoria_referencial')

        request.session['subtrajetoria_referencial'] = id_referencial

        globais = request.session['globais']

        print(globais['trajetos'])
      
        return render(request, 'trajesim/passo4.html', {
            'passo': 4,
            'global': globais
        })
