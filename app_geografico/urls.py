from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static
from app_geografico import views

urlpatterns = patterns('',
    # url(r'^$', TemplateView.as_view(template_name='app_bovino/index.html'), name='index'),
    # url(r'^$', views.index, name='index'),
)

if not settings.DEBUG:
      urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
      urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
