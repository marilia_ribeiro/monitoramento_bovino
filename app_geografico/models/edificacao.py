#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models

from app_geografico.models.loteamento import Loteamento


class Edificacao(models.Model):
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')
    delimitacao = models.PolygonField()
    loteamento = models.ForeignKey(Loteamento)

    # objects = models.GeoManager()

    def __unicode__(self):
        return self.descricao
