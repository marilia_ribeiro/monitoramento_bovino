#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.db import models
from django.contrib.gis.db import models
from tipo_via import TipoVia
from propriedade import Propriedade

class Via(models.Model):
    via_linha = models.LineStringField()
    via_poligono = models.PolygonField()
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')
    tipo_via = models.ForeignKey(TipoVia)
    propriedade = models.ForeignKey(Propriedade)

    # objects = models.GeoManager()

    def __unicode__(self):
        return self.via_poligono
