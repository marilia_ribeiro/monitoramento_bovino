var latitude = -26.3951021;
var longitude = -48.740758;
var mapa;
var propriedade;
// var trajetorias = [[[-26.39093815, -48.74154689], [-26.39091797, -48.7415659]], [[-26.3912464, -48.74164834], [-26.39150406, -48.74174312]]];
var trajetorias;
var marcadores = new Array();

$(document).ready(function(){
    mapa = mostraMapa('#map', latitude, longitude);

    propriedade = JSON.parse(document.getElementById("propriedade").value);
    desenhaPoligono(mapa, propriedade, '#000000', 0.3, 2, 'grey', 0.4);


    trajetorias = JSON.parse(document.getElementById("trajetorias").value);
    plotaTrajetorias(trajetorias, mapa);


    for (var i in marcadores){
        console.log(marcadores[i]);
    }
    var markerCluster = new MarkerClusterer(mapa,marcadores);
    // marcaCluster(mapa);

});

function plotaTrajetorias(trajetorias, mapa) {
    for (var i in trajetorias){
        desenhaLinestring(mapa, trajetorias[i], defineCor(), 0.6, 1);
    }
}

function defineCor(){
    caracteres = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
    var cor = "#";
    for (i=0;i<6;i++){
       cor += caracteres[Math.floor(Math.random() * (caracteres.length))];
    }
    return cor;
}

function mostraMapa(elemento, latitude, longitude){
  return new GMaps({
     el: elemento,
     lat: latitude,
     lng: longitude
  });
}

function desenhaPoligono(mapa, coordenadas, corLinha, opacidadeLinha, espessuraLinha, corPoligono, opacidadePoligono){
  return mapa.drawPolygon({
    paths: coordenadas,
    strokeColor: corLinha,
    strokeOpacity: opacidadeLinha,
    strokeWeight: espessuraLinha,
    fillColor: corPoligono,
    fillOpacity: opacidadePoligono
  });
}

function desenhaLinestring(mapa, coordenadas, corLinha, opacidadeLinha, espessuraLinha){
  return mapa.drawPolyline({
      path: coordenadas,
      strokeColor: corLinha,
      strokeOpacity: opacidadeLinha,
      strokeWeight: espessuraLinha
  });
}

