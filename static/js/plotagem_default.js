var objetos_pares = new Array();
var marcadores = new Array();

function plota_poligono(array){
	poligono = new google.maps.Polygon({
        paths: poligono_plotavel,
        strokeColor: 'red',
        strokeOpacity: 0.3,
        strokeWeight: 2,
        fillColor: 'red',
        fillOpacity: 0.05
    });
    poligono.setMap(mapa);
}

function ponto_inicial_comum(coordenadas,titulo){
    var ponto_inicial = new google.maps.Marker({
        position: coordenadas,
        title: titulo,
        icon: 'http://maps.gstatic.com/intl/en_ALL/mapfiles/dd-start.png',
        map: mapa
    });

    marcadores.push(ponto_inicial);
}

function plota_trajetoria(trajetoria,cor,posicao,opacidade){
	ponto_inicial_comum(trajetoria[0],'Trajetória');

	var trajeto = new google.maps.Polyline({
        path: trajetoria,
        geodesic: false,
        strokeColor: cor,
        strokeOpacity: opacidade,
        strokeWeight: 2
    });

    trajeto.setMap(mapa);

    if(posicao!=-1){
    	objetos_pares[posicao].push(trajeto);
    }
}