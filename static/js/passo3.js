var trajetoriasObj = new Array();
var marcadores = new Array();

$(document).ready(function(){

    $('.standard.test.modal').modal('show');
    
    identificaTrajetorias();
    identificaPoligono();
    //var markerCluster = new MarkerClusterer(mapa,marcadores);
})

function identificaTrajetorias(){
    trajetorias = $('#trajetorias_encontradas').val().split(";");
    ids = $('#ids_das_trajetorias_encontradas').val().split(";");

    for(var i in trajetorias){
        trajetoria = trajetorias[i];
        id = ids[i];
        
        if(trajetoria.length>0){
            trajetoria_plotavel = identificaCoordenadas(trajetoria);
            plotaTrajetoria(trajetoria_plotavel,id);
        }
    }
}

function identificaCoordenadas(trajetoria){
    var trajetoria_plotavel = new Array ();
    pontos = trajetoria.split(" ");

    for(var p in pontos){
        ponto = pontos[p].split(",");
        latitude = ponto[0];
        longitude = ponto[1];

        trajetoria_plotavel.push(new google.maps.LatLng(latitude,longitude));
    }
    
    return trajetoria_plotavel;
}


function plotaTrajetoria(trajetoria,id){
    cor = defineCor();

    plotaPontosExtremosTrajetoria(trajetoria[0],id,trajetoriasObj.length);
    
    if(typeof trajetoria)
    var trajeto = new google.maps.Polyline({
        path: trajetoria,
        geodesic: false,
        strokeColor: cor,
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    trajetoriasObj.push(trajeto);

    trajeto.setMap(mapa);
}

function plotaPontosExtremosTrajetoria(pontoinicial,id,posicao){
    plotaPonto(pontoinicial,"i",id,posicao);
}

function plotaPonto(coordenadas,titulo,id,posicao){
    var corOriginal;

    if(titulo == "i"){
        var icone = 'dd-start.png';
        var titulo = 'Início da trajetória: '+id;
    }

    var ponto = new google.maps.Marker({
      position: coordenadas,
      map: mapa,
      title: titulo,
      icon: 'http://maps.gstatic.com/intl/en_ALL/mapfiles/'+icone
    });

    marcadores.push(ponto);

    google.maps.event.addListener(ponto, 'click', function(){
        defineComoReferencial(id,corOriginal);
    });

    google.maps.event.addListener(ponto, 'mouseover', function(){
        corOriginal = trajetoriasObj[posicao].strokeColor;
        destacaTrajetoria(posicao,3,'destaca');
    });

    google.maps.event.addListener(ponto, 'mouseout', function(){
        destacaTrajetoria(posicao,2,'');
    });
}

function destacaTrajetoria(posicao,espessura,condicao){
    for(var t in trajetoriasObj){
        if(condicao == 'destaca'){
            if(t == posicao){
                trajetoriasObj[posicao].setOptions({strokeWeight: espessura});
            }else{
                trajetoriasObj[t].setOptions({strokeOpacity: 0.2});   
            }
        }else{
            trajetoriasObj[t].setOptions({strokeOpacity: 1, strokeWeight: espessura}); 
        }
    }
}

function defineComoReferencial(id,cor){
    $('#id_trajetoria_referencial').val(id);
    $('#cor_trajetoria_referencial').css('background-color',cor);
    $('#submitPasso2').removeClass('disabled');
}
