# -*- coding: utf-8 -*-
from django import template
from datetime import datetime

register = template.Library()


@register.filter(name='calcula_idade')
def calcula_idade(data_nascimento):
    hoje = datetime.now()
    return hoje.year - data_nascimento.year - ((hoje.month, hoje.day) < (data_nascimento.month, data_nascimento.day))