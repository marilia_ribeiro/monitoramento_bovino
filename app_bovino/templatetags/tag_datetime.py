# -*- coding: utf-8 -*-
from django import template
from datetime import datetime, timedelta

register = template.Library()


@register.filter(name='calcula_tempo_total_horas')
def calcula_tempo_total_horas(tempo_duracao):
    tempo_segundos = calcula_tempo_total_segundos(tempo_duracao)
    horas = tempo_segundos // 3600
    minutos = (tempo_segundos % 3600) // 60
    segundos = (tempo_segundos % 3600) % 60
    print "%d:%d:%d" % (horas, minutos, segundos)

@register.filter(name='calcula_tempo_total_segundos')
def calcula_tempo_total_segundos(tempo):
    return tempo.total_seconds()