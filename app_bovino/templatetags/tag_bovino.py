# -*- coding: utf-8 -*-
from django import template
from datetime import datetime, timedelta
from app_bovino.models import StatusReprodutivo, BovinoStatusReprodutivo, Lote, BovinoLote, Categoria, BovinoCategoria, Bovino

register = template.Library()

@register.filter(name='calcula_duracao_prevista_status_reprodutivo')
def calcula_duracao_prevista_status_reprodutivo(status_reprodutivo, bovino):
    status_reprodutivo = StatusReprodutivo.objects.filter(status_reprodutivo=status_reprodutivo)[0]
    data = BovinoStatusReprodutivo.objects.filter(bovino=bovino.id, status_reprodutivo=status_reprodutivo.id)[0].data

    if status_reprodutivo.periodo_duracao_previsto:
        return data.date() + timedelta(status_reprodutivo.periodo_duracao_previsto)


@register.filter(name='calcula_duracao_padrao_status_reprodutivo')
def calcula_duracao_padrao_status_reprodutivo(status_reprodutivo, bovino):
    status_reprodutivo = StatusReprodutivo.objects.filter(status_reprodutivo=status_reprodutivo)[0]
    data = BovinoStatusReprodutivo.objects.filter(bovino=bovino.id, status_reprodutivo=status_reprodutivo.id)[0].data

    if status_reprodutivo.periodo_duracao_padrao:
        return data.date() + timedelta(status_reprodutivo.periodo_duracao_padrao)


@register.filter(name='calcula_duracao_prevista_lote')
def calcula_duracao_prevista_lote(lote, bovino):
    lote = Lote.objects.filter(lote=lote)[0]
    data = BovinoLote.objects.filter(bovino=bovino.id, lote=lote.id)[0].data

    if lote.periodo_duracao_previsto:
        return data.date() + timedelta(lote.periodo_duracao_previsto)


@register.filter(name='calcula_duracao_padrao_lote')
def calcula_duracao_padrao_lote(lote, bovino):
    lote = Lote.objects.filter(lote=lote)[0]
    data = BovinoLote.objects.filter(bovino=bovino.id, lote=lote.id)[0].data

    if lote.periodo_duracao_padrao:
        return data.date() + timedelta(lote.periodo_duracao_padrao)


@register.filter(name='calcula_duracao_prevista_categoria')
def calcula_duracao_prevista_categoria(categoria, bovino):
    categoria = Categoria.objects.filter(categoria=categoria)[0]
    data = BovinoCategoria.objects.filter(bovino=bovino.id, categoria=categoria.id)[0].data

    if categoria.periodo_duracao_previsto:
        return data.date() + timedelta(categoria.periodo_duracao_previsto)


@register.filter(name='calcula_duracao_padrao_categoria')
def calcula_duracao_padrao_categoria(categoria, bovino):
    categoria = Categoria.objects.filter(categoria=categoria)[0]
    data = BovinoCategoria.objects.filter(bovino=bovino.id, categoria=categoria.id)[0].data

    if categoria.periodo_duracao_padrao:
        return data.date() + timedelta(categoria.periodo_duracao_padrao)

@register.filter(name='getBovino')
def getBovino(id):
    return Bovino.objects.get(id=id)

@register.filter(name='getNome')
def getNome(id):
    return getBovino(id).nome

@register.filter(name='getBrinco')
def getBrinco(id):
    return getBovino(id).brinco

@register.filter(name='getIsFemea')
def getIsFemea(id):
    return getBovino(id).is_femea