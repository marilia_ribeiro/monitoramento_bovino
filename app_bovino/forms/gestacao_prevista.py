from django import forms
from app_bovino.models import Gestacao

class FormGestacaoPrevista(forms.ModelForm):
    class Meta:
        model = Gestacao
        fields = ('data_prevista_diagnostico_gestacao',)