from django import forms
from app_bovino.models import Esperma

class FormEsperma(forms.ModelForm):
    class Meta:
        model = Esperma
        # widgets = {'data_nascimento': forms.DateInput(attrs={'class': 'datepicker'})}
        fields = "__all__"
