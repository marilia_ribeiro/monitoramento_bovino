from betterforms.multiform import MultiModelForm
from app_bovino.forms import FormInseminacaoNulo, FormGestacaoDiagnostico

class InseminacaoConfirmarMultiForm(MultiModelForm):
    form_classes = {
        'inseminacao': FormInseminacaoNulo,
        'gestacao': FormGestacaoDiagnostico,
    }