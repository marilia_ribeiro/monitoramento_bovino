from django import forms
from app_bovino.models import Lote

class FormLote(forms.ModelForm):
    class Meta:
        model = Lote
        fields = "__all__"
