#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django import forms
from app_bovino.models.bovino import Bovino,SEXOS
from app_bovino.models.raca import Raca
from app_bovino.models.lote import Lote
from app_bovino.models.categoria import Categoria
from app_bovino.models.status_reprodutivo import StatusReprodutivo
from app_bovino.models.esperma import Esperma


class FormBovino(forms.ModelForm):

    class Meta:
        model = Bovino
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(FormBovino, self).__init__(*args, **kwargs)

        racas = Raca.objects.all()
        lotes = Lote.objects.all()
        categorias = Categoria.objects.all()
        status_reprodutivo = StatusReprodutivo.objects.all()
        pai = Esperma.objects.all()
        mae = Bovino.objects.filter(is_femea=True)

        self.fields['is_femea'].widget = forms.Select(
            choices=SEXOS,
            attrs={'class': 'ui dropdown'})

        self.fields['raca'].widget = forms.Select(
            choices=racas.values_list('id', 'raca'),
            attrs={'class': 'ui dropdown'})

        self.fields['lote'].widget = forms.SelectMultiple(
            choices=lotes.values_list('id','lote'),
            attrs={'class': 'ui dropdown'})

        self.fields['categoria'].widget = forms.SelectMultiple(
            choices=categorias.values_list('id', 'categoria'),
            attrs={'class': 'ui dropdown'})

        self.fields['status_reprodutivo'].widget = forms.SelectMultiple(
            choices=status_reprodutivo.values_list('id', 'status_reprodutivo'),
            attrs={'class': 'ui dropdown'})

        self.fields['pai'].widget = forms.Select(
            choices=pai.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

        self.fields['mae'].widget = forms.Select(
            choices=mae.values_list('id', 'nome'),
            attrs={'class': 'ui dropdown'})

class BovinoStatusReprodutivoForm(forms.ModelForm):

    class Meta:
        model = Bovino
        fields = ('status_reprodutivo',)

    def __init__(self, *args, **kwargs):
        super(BovinoStatusReprodutivoForm, self).__init__(*args, **kwargs)

        status_reprodutivo = StatusReprodutivo.objects.all()

        self.fields['status_reprodutivo'].widget = forms.SelectMultiple(
            choices=status_reprodutivo.values_list('id', 'status_reprodutivo'),
            attrs={'class': 'ui dropdown'})


class BovinoLoteForm(forms.ModelForm):

    class Meta:
        model = Bovino
        fields = ('lote',)

    def __init__(self, *args, **kwargs):
        super(BovinoLoteForm, self).__init__(*args, **kwargs)

        lotes = Lote.objects.all()

        self.fields['lote'].widget = forms.SelectMultiple(
            choices=lotes.values_list('id', 'lote'),
            attrs={'class': 'ui dropdown'})

class BovinoCategoriaForm(forms.ModelForm):

    class Meta:
        model = Bovino
        fields = ('categoria',)

    def __init__(self, *args, **kwargs):
        super(BovinoCategoriaForm, self).__init__(*args, **kwargs)

        categorias = Categoria.objects.all()

        self.fields['categoria'].widget = forms.SelectMultiple(
            choices=categorias.values_list('id', 'categoria'),
            attrs={'class': 'ui dropdown'})
