from django import forms
from app_bovino.models import Raca


class FormRaca(forms.ModelForm):
    class Meta:
        model = Raca
        fields = "__all__"