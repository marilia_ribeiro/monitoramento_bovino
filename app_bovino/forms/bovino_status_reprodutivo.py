from django import forms
from app_bovino.models import BovinoStatusReprodutivo

class FormBovinoStatusReprodutivo(forms.ModelForm):
    class Meta:
        model = BovinoStatusReprodutivo
        fields = "__all__"
