from django import forms
from app_bovino.models import BovinoLote

class FormBovinoLote(forms.ModelForm):    
    class Meta:
        model = BovinoLote
        fields = "__all__"
