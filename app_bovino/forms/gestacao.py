from django import forms
from app_bovino.models import Gestacao

class FormGestacao(forms.ModelForm):
    class Meta:
        model = Gestacao
        fields = "__all__"

    #
    # def __init__(self, *args, **kwargs):
    #     super(FormGestacao, self).__init__(*args, **kwargs)
    #     inseminacoes = Gestacao.objects.filter(inseminacao__status_inseminacao=False)
    #
    #     self.fields['inseminacao'].widget = forms.Select(
    #         choices=inseminacoes.values_list('id', 'inseminacao__vaca__nome'),
    #         attrs={'class': 'ui dropdown'})

