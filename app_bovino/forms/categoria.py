from django import forms
from app_bovino.models import Categoria

class FormCategoria(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = "__all__"
