from django import forms
from app_bovino.models import BovinoCategoria

class FormBovinoCategoria(forms.ModelForm):
    class Meta:
        model = BovinoCategoria
        fields = "__all__"
