# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from app_bovino.models import Categoria, Lote, Raca, StatusReprodutivo, Bovino, BovinoLote, BovinoCategoria, BovinoStatusReprodutivo
from django.contrib.auth.models import User


class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _cadastrarCategoria(self):
        categoria1 = Categoria(categoria='Vaca', descricao='Vaca')
        categoria1.save()
        
        categoria2 = Categoria(categoria='Touro', descricao='Touro')
        categoria2.save()

        categoria3 = Categoria(categoria='Novilha', descricao='Novilha')
        categoria3.save()

        categoria4 = Categoria(categoria='Novilho', descricao='Novilho')
        categoria4.save()

        categoria5 = Categoria(categoria='Terneira', descricao='Terneira')
        categoria5.save()

        categoria6 = Categoria(categoria='Terneiro', descricao='Terneiro')
        categoria6.save()

        # categoria.periodo_duracao_previsto = periodo_duracao_previsto
        # categoria.periodo_duracao_padrao = periodo_duracao_padrao

    def _cadastrarLote(self):
        lote1 = Lote(lote='Cria', descricao='Cria')
        lote1.save()

        lote2 = Lote(lote='Ho/Je', descricao='Ho/Je')
        lote2.save()

        lote3 = Lote(lote='Lactação', descricao='Lactação')
        lote3.save()

        lote4 = Lote(lote='Novilha', descricao='Novilha')
        lote4.save()

        lote5 = Lote(lote='Seca', descricao='Seca')  # previsao_parto - 60
        lote5.save()

        lote6 = Lote(lote='X', descricao='X')
        lote6.save()

        # lote.periodo_duracao_previsto = periodo_duracao_previsto
        # lote.periodo_duracao_padrao = periodo_duracao_padrao

    def _cadastrarRaca(self):
        raca1 = Raca(raca='Holandês', descricao='Holandês')
        raca1.save()

        raca2 = Raca(raca='Jersey', descricao='Jersey')
        raca2.save()

        raca3 = Raca(raca='Ho/Je', descricao='Ho/Je')
        raca3.save()


    def _cadastrarStatusReprodutivo(self):
        status1 = StatusReprodutivo(status_reprodutivo='Possível Sincronização',
                                    descricao='Possível Sincronização',
                                    periodo_duracao_previsto=1)
        status1.save()

        status2 = StatusReprodutivo(status_reprodutivo='Prenha',
                                    descricao='Prenha',
                                    periodo_duracao_previsto=280)
        status2.save()

        status3 = StatusReprodutivo(status_reprodutivo='A confirmar',
                                    descricao='A confirmar',
                                    periodo_duracao_previsto=35)
        status3.save()

        status4 = StatusReprodutivo(status_reprodutivo='Vazia',
                                    descricao='Vazia',
                                    periodo_duracao_previsto=90)
        status4.save()

        status5 = StatusReprodutivo(status_reprodutivo='SC Sincronizada',
                                    descricao='SC Sincronizada',
                                    periodo_duracao_previsto=1)
        status5.save()

        status6 = StatusReprodutivo(status_reprodutivo='Pós parto',
                                    descricao='Pós parto',
                                    periodo_duracao_previsto=7)
        status6.save()

        # status_reprodutivo.periodo_duracao_padrao = periodo_duracao_padrao

    def _cadastrarBovino(self):
        betinha = Bovino(nome='Betinha',
                         brinco='501893',
                         is_femea=True,
                         data_nascimento='2014-07-17',
                         data_cadastro='2017-08-07',
                         raca=Raca.objects.get(pk=1),
                         # pessoa=User.objects.get(username='admin')
                         )
        betinha.save()

        dayri = Bovino(nome='Dayri',
                       brinco='569717',
                       is_femea=True,
                       data_nascimento='2016-06-11',
                       data_cadastro='2017-08-07',
                       raca=Raca.objects.get(pk=3),
                       # pessoa=User.objects.get(username='admin')
                       )
        dayri.save()

        tangerina = Bovino(nome='Tangerina',
                           brinco='4392874',
                           is_femea=True,
                           data_nascimento='2009-07-17',
                           data_cadastro='2009-07-17',
                           raca=Raca.objects.get(pk=1),
                           # pessoa=User.objects.get(username='admin')
                           )
        tangerina.save()

        dilma = Bovino(nome='Dilma',
                       brinco='09343',
                       is_femea=True,
                       data_nascimento='2010-07-17',
                       data_cadastro='2010-07-17',
                       raca=Raca.objects.get(pk=1),
                       mae=Bovino.objects.get(nome='Tangerina'),
                       # pessoa=User.objects.get(username='admin')
                       )
        dilma.save()

    def _cadastrarBovinoLote(self):
        betinha_lote = BovinoLote(bovino=Bovino.objects.get(nome='Betinha'),
                                  lote=Lote.objects.get(pk=5),
                                  data='2017-08-07')
        betinha_lote.save()

        dayri_lote = BovinoLote(bovino=Bovino.objects.get(nome='Dayri'),
                                 lote=Lote.objects.get(pk=1),
                                 data='2017-08-07')
        dayri_lote.save()

        tangerina = BovinoLote(bovino=Bovino.objects.get(nome='Tangerina'),
                                lote=Lote.objects.get(pk=6),
                                data='2009-01-17')
        tangerina.save()

        dilma = BovinoLote(bovino=Bovino.objects.get(nome='Dilma'),
                               lote=Lote.objects.get(pk=6),
                               data='2010-07-17')
        dilma.save()



    def _cadastrarBovinoCategoria(self):
        betinha_categoria = BovinoCategoria(bovino=Bovino.objects.get(nome='Betinha'),
                                            categoria=Categoria.objects.get(pk=1),
                                            data='2017-08-07')
        betinha_categoria.save()

        dayri_categoria = BovinoCategoria(bovino=Bovino.objects.get(nome='Dayri'),
                                          categoria=Categoria.objects.get(pk=5),
                                          data='2017-08-07')
        dayri_categoria.save()

        tangerina = BovinoCategoria(bovino=Bovino.objects.get(nome='Tangerina'),
                                            categoria=Categoria.objects.get(pk=1),
                                            data='2009-07-17')
        tangerina.save()

        dilma = BovinoCategoria(bovino=Bovino.objects.get(nome='Dilma'),
                                    categoria=Categoria.objects.get(pk=1),
                                    data='2010-07-17')
        dilma.save()

    def _editarBovino(self):
        betinha = Bovino.objects.get(nome='Betinha')
        betinha.lote.add(Lote.objects.get(pk=5))
        betinha.categoria.add(Categoria.objects.get(pk=1))
        betinha.save()

        dayri = Bovino.objects.get(nome='Dayri')
        dayri.lote.add(Lote.objects.get(pk=1))
        dayri.categoria.add(Categoria.objects.get(pk=5))
        dayri.save()

        tangerina = Bovino.objects.get(nome='Tangerina')
        tangerina.lote.add(Lote.objects.get(pk=6))
        tangerina.categoria.add(Categoria.objects.get(pk=1))
        tangerina.save()

        dilma = Bovino.objects.get(nome='Dilma')
        dilma.lote.add(Lote.objects.get(pk=6))
        dilma.categoria.add(Categoria.objects.get(pk=1))
        dilma.save()

    def handle(self, *args, **options):
        self._cadastrarCategoria()
        self._cadastrarLote()
        self._cadastrarRaca()
        self._cadastrarStatusReprodutivo()
        self._cadastrarBovino()
        self._cadastrarBovinoLote()
        self._cadastrarBovinoCategoria()
        self._editarBovino()
