from django.contrib import admin
from app_bovino.models import Raca, Lote, Categoria, StatusReprodutivo, Bovino, BovinoLote, BovinoCategoria, BovinoStatusReprodutivo, Esperma, Inseminacao, Gestacao, Descarte,Pessoa

admin.site.register(Raca)
admin.site.register(Lote)
admin.site.register(Categoria)
admin.site.register(StatusReprodutivo)
admin.site.register(Bovino)
admin.site.register(BovinoLote)
admin.site.register(BovinoCategoria)
admin.site.register(BovinoStatusReprodutivo)
admin.site.register(Esperma)
admin.site.register(Inseminacao)
admin.site.register(Gestacao)
admin.site.register(Descarte)
admin.site.register(Pessoa)
