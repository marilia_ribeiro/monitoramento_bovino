#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from inseminacao import Inseminacao

class Gestacao(models.Model):
    inseminacao = models.ForeignKey(Inseminacao, blank=False, help_text='Descrição da inseminação:', verbose_name=u'Preenchimento obrigatório. Contém os campos data da inseminação, nome da vaca e nome do touro que foram cadastrados no formulário de inseminação.')
    data_hora = models.DateTimeField(auto_now_add=True)
    data_prevista_diagnostico_gestacao = models.DateField(blank=True, null=True, help_text='Data prevista do diagnostico da gestação:')
    data_diagnostico_gestacao = models.DateField(blank=True, null=True, help_text='Data do diagnostico da gestação:')
    data_prevista_parto = models.DateField(blank=True, null=True, help_text='Data prevista do parto:')
    data_parto = models.DateField(blank=True, null=True, help_text='Data do parto:')
    data_ultimo_parto = models.DateField(blank=True, null=True, help_text='Data do último parto:')
    data_prevista_secar = models.DateField(blank=True, null=True, help_text='Data prevista para secar:')
    data_secar = models.DateField(blank=True, null=True, help_text='Data em que secou:')
    dias_pos_ultimo_parto = models.IntegerField(max_length=None, blank=True, null=True,  help_text='Dias após o último parto:')
    del_dias_em_lactacao = models.IntegerField(max_length=None, blank=True, null=True, help_text='DEL:')
    ipc = models.IntegerField(max_length=None, blank=True, null=True, help_text='IPC:')
    observacao = models.CharField(max_length=255, blank=True, null=True, help_text='Observação:')

    def __unicode__(self):
        return self.inseminacao.vaca.nome
