#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from bovino import Bovino
from lote import Lote

class BovinoLote(models.Model):
    bovino = models.ForeignKey(Bovino, on_delete=models.CASCADE)
    lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)

    class Meta:
        auto_created = True
