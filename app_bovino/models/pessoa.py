#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User

from monitoramento_bovino.settings import MEDIA_URL


class Pessoa(models.Model):
    '''Model UserProfile'''
    #campo obrigatório - relaciona com model User do django
    user = models.OneToOneField(User)

    #campos adicionais
    cpf = models.CharField(max_length=11, blank=True, help_text='CPF:')
    telefone = models.CharField(max_length=50, blank=True, help_text='Telefone:',)
    foto = models.ImageField(upload_to='profile_images', blank=True)
    # foto = models.ImageField(upload_to=MEDIA_URL + 'profile_images', blank=True)

    def __unicode__(self):
        return self.user.get_full_name()
