#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

class StatusReprodutivo(models.Model):
    status_reprodutivo = models.CharField(max_length=128,  blank=False, help_text='Status Reprodutivo:')
    descricao = models.CharField(max_length=255,  blank=True, help_text='Descrição:')
    periodo_duracao_previsto = models.IntegerField(max_length=None, blank=True, null= True, help_text='Perído de duração previsto (dias):')
    periodo_duracao_padrao = models.IntegerField(max_length=None, blank=True, null= True, help_text='Período de duração padrão (dias):')

    def __unicode__(self):
        return self.status_reprodutivo
