#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormStatusReprodutivo
from app_bovino.models import StatusReprodutivo

#@login_required
def cadastrarStatusReprodutivo(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormStatusReprodutivo(request.POST)

        # forms é valido
        if form.is_valid():
            status_reprodutivo = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarStatusReprodutivo, args=(status_reprodutivo.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormStatusReprodutivo()

    return render(request, 'app_bovino/cadastrar/status_reprodutivo.html', {'form': form})

def visualizarStatusReprodutivo(request, id):
    status_reprodutivo = StatusReprodutivo.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/status_reprodutivo.html', {'status_reprodutivo': status_reprodutivo})

def editarStatusReprodutivo(request, id):
    if request.method == 'POST':
        if id:
            status_reprodutivo = StatusReprodutivo.objects.get(id=id)
            form = FormStatusReprodutivo(request.POST,instance=status_reprodutivo)
        else:
            form = FormStatusReprodutivo(request.POST)
        if form.is_valid():
            status_reprodutivo = form.save()
            return HttpResponseRedirect(reverse(visualizarStatusReprodutivo, args=(status_reprodutivo.id,)))
    else:
        if id:
            status_reprodutivo = StatusReprodutivo.objects.get(id=id)
            form = FormStatusReprodutivo(instance=status_reprodutivo)

    return render(request, 'app_bovino/editar/status_reprodutivo.html', {'form':form, 'status_reprodutivo': status_reprodutivo})

def excluirStatusReprodutivo(request, id):
    status_reprodutivo = StatusReprodutivo.objects.get(id=id)

    if request.method == 'POST':
        status_reprodutivo.delete()
        return listarStatusReprodutivo(request)

    return render(request, 'app_bovino/excluir/status_reprodutivo.html', {'status_reprodutivo': status_reprodutivo})

def listarStatusReprodutivo(request):
    status_reprodutivo = StatusReprodutivo.objects.all()
    return render(request, 'app_bovino/listar/status_reprodutivo.html', {'status_reprodutivo': status_reprodutivo})
