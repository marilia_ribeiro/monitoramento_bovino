#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormCategoria
from app_bovino.models import Categoria

#@login_required
def cadastrarCategoria(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormCategoria(request.POST)

        # forms é valido
        if form.is_valid():
            categoria = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarCategoria, args=(categoria.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormCategoria()

    return render(request, 'app_bovino/cadastrar/categoria.html', {'form': form})

def visualizarCategoria(request, id):
    categoria = Categoria.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/categoria.html', {'categoria': categoria})

def editarCategoria(request, id):
    if request.method == 'POST':
        if id:
            categoria = Categoria.objects.get(id=id)
            form = FormCategoria(request.POST,instance=categoria)
        else:
            form = FormCategoria(request.POST)
        if form.is_valid():
            categoria = form.save()
            return HttpResponseRedirect(reverse(visualizarCategoria, args=(categoria.id,)))
    else:
        if id:
            categoria = Categoria.objects.get(id=id)
            form = FormCategoria(instance=categoria)

    return render(request, 'app_bovino/editar/categoria.html', {'form':form, 'categoria': categoria})

def excluirCategoria(request, id):
    categoria = Categoria.objects.get(id=id)

    if request.method == 'POST':
        categoria.delete()
        return listarCategoria(request)

    return render(request, 'app_bovino/excluir/categoria.html', {'categoria': categoria})

def listarCategoria(request):
    categorias = Categoria.objects.all()
    return render(request, 'app_bovino/listar/categoria.html', {'categorias': categorias})
