#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import *
from django.utils import timezone

from app_bovino.forms import FormBovino, BovinoStatusReprodutivoForm, BovinoLoteForm, BovinoCategoriaForm
from app_bovino.models import Bovino, Lote, BovinoLote, Categoria, BovinoCategoria, StatusReprodutivo, BovinoStatusReprodutivo

#@login_required
def cadastrarBovino(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormBovino(request.POST)

        if form.is_valid():
            bovino = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarBovino, args=(bovino.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormBovino()

    return render(request, 'app_bovino/cadastrar/bovino.html', {'form': form})

def visualizarBovino(request, id):
    bovino = Bovino.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/bovino.html', {'bovino': bovino})

def StatusReprodutivoBovinoDetailView(request, pk):
    bovino = Bovino.objects.get(id=pk)
    return render(request, 'app_bovino/visualizar/status_reprodutivo_bovino.html', {'bovino': bovino})

def LoteBovinoDetailView(request, pk):
    bovino = Bovino.objects.get(id=pk)
    return render(request, 'app_bovino/visualizar/lote_bovino.html', {'bovino': bovino})

def CategoriaBovinoDetailView(request, pk):
    bovino = Bovino.objects.get(id=pk)
    return render(request, 'app_bovino/visualizar/categoria_bovino.html', {'bovino': bovino})

def editarBovino(request, id):
    if request.method == 'POST':
        if id:
            bovino = Bovino.objects.get(id=id)
            form = FormBovino(request.POST,instance=bovino)
        else:
            form = FormBovino(request.POST)
        if form.is_valid():
            bovino = form.save()
            return HttpResponseRedirect(reverse(visualizarBovino, args=(bovino.id,)))
    else:
        if id:
            bovino = Bovino.objects.get(id=id)
            form = FormBovino(instance=bovino)

    return render(request, 'app_bovino/editar/bovino.html', {'form':form, 'bovino': bovino})

def excluirBovino(request, id):
    bovino = Bovino.objects.get(id=id)

    if request.method == 'POST':
        bovino.delete()
        return listarBovino(request)

    return render(request, 'app_bovino/excluir/bovino.html', {'bovino': bovino})

def listarBovino(request):
    bovinos = Bovino.objects.all()
    return render(request, 'app_bovino/listar/bovino.html', {'bovinos': bovinos})

def StatusReprodutivoBovinosListView(request):
    bovinos = Bovino.objects.all()
    return render(request, 'app_bovino/listar/status_reprodutivo_bovino.html', {'bovinos': bovinos})

def LoteBovinosListView(request):
    bovinos = Bovino.objects.all()
    return render(request, 'app_bovino/listar/lote_bovino.html', {'bovinos': bovinos})

def CategoriaBovinosListView(request):
    bovinos = Bovino.objects.all()
    return render(request, 'app_bovino/listar/categoria_bovino.html', {'bovinos': bovinos})

class StatusReprodutivoBovinoUpdateView(UpdateView):
    template_name = 'app_bovino/editar/status_reprodutivo_bovino.html'
    model = Bovino
    form_class = BovinoStatusReprodutivoForm

    def form_valid(self, form):
        form.save(commit=True)

        return super(StatusReprodutivoBovinoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('visualizar_status_reprodutivo_bovino', kwargs={'id': self.get_object().id})

class LoteBovinoUpdateView(UpdateView):
    template_name = 'app_bovino/editar/lote_bovino.html'
    model = Bovino
    form_class = BovinoLoteForm

    def form_valid(self, form):
        form.save(commit=True)

        return super(LoteBovinoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('visualizar_lote_bovino', kwargs={'id': self.get_object().id})

class CategoriaBovinoUpdateView(UpdateView):
    template_name = 'app_bovino/editar/categoria_bovino.html'
    model = Bovino
    form_class = BovinoCategoriaForm

    def form_valid(self, form):
        form.save(commit=True)

        return super(CategoriaBovinoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('visualizar_categoria_bovino', kwargs={'id': self.get_object().id})