#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.views.generic import FormView, UpdateView, View

from app_bovino.forms import FormInseminacao, FormInseminacaoEditar, FormGestacaoDiagnostico
from app_bovino.models import Inseminacao, Gestacao
from django.core.urlresolvers import reverse_lazy
from app_bovino.templatetags.tag_inseminacao import *

STATUS_REPRODUTIVO_CONFIRMAR = 'A confirmar'
STATUS_REPRODUTIVO_PRENHA = 'Prenha'
PREVISAO_CIO = 21
PREVISAO_SECAR = 60

class InseminacaoCreateView(FormView):
    template_name = 'app_bovino/cadastrar/inseminacao.html'
    form_class = FormInseminacao

    def form_valid(self, form):
        inseminacao = form.save(commit=False)
        inseminacao.data_prevista_cio = calcula_data_prevista_cio(inseminacao.data_inseminacao)
        inseminacao.save()

        bovino = Bovino.objects.get(id=inseminacao.vaca.id)
        bovino.status_reprodutivo = StatusReprodutivo.objects.filter(status_reprodutivo=STATUS_REPRODUTIVO_CONFIRMAR)
        bovino.save()

        self.object = inseminacao
        return redirect(self.get_success_url())


    def get_success_url(self, **kwargs):
            return reverse_lazy('visualizar_inseminacao', args=(self.object.id,))


class InseminacaoUpdateView(UpdateView):
    template_name = 'app_bovino/editar/inseminacao.html'
    model = Inseminacao
    form_class = FormInseminacaoEditar

    def form_valid(self, form):
        self.object = form.save(commit=True)
        return super(InseminacaoUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('visualizar_inseminacao', kwargs={'id': self.get_object().id})


class ConfirmarInseminacaUpdateView(View):
    template = 'app_bovino/editar/diagnostico_gestacao.html'
    context = {}

    def get(self, request, inseminacao_id=None):
        self.inseminacao = self.getInseminacao(inseminacao_id)
        if self.inseminacao:
            self.context["form"] = FormGestacaoDiagnostico()
            self.context["inseminacao"] = self.inseminacao
            return render(request, self.template, self.context)

    def post(self, request, inseminacao_id=None):
        self.inseminacao = self.getInseminacao(inseminacao_id)
        if self.inseminacao:
            form = FormGestacaoDiagnostico(request.POST)

            if form.is_valid():
                self.inseminacao.status_inseminacao = True
                self.inseminacao.save()

                self.bovino = Bovino.objects.get(id=self.inseminacao.vaca.id)
                self.bovino.status_reprodutivo = StatusReprodutivo.objects.filter(
                    status_reprodutivo=STATUS_REPRODUTIVO_PRENHA)
                self.bovino.save()

                self.gestacao = form.save(commit=False)
                self.gestacaoCreateView(self.gestacao, self.inseminacao, self.bovino)
                return redirect(self.get_success_url())
            else:
                self.context["form"] = form
                self.context["inseminacao"] = self.getInseminacao(inseminacao_id)
            return render(request, self.template, self.context)



    def getInseminacao(self, inseminacao_id=None):
        return Inseminacao.objects.get(id=inseminacao_id)

    def gestacaoCreateView(self, gestacao, inseminacao, bovino):
        gestacao.inseminacao = inseminacao
        gestacao.data_prevista_diagnostico_gestacao = calcula_data_prevista_diagnostico_gestacao(inseminacao.data_inseminacao)
        gestacao.data_prevista_parto = calcula_data_prevista_parto(inseminacao.data_inseminacao)
        gestacao.data_prevista_secar = calcula_data_prevista_secar(inseminacao.data_inseminacao)
        gestacao.data_ultimo_parto = get_data_ultimo_parto(bovino)
        gestacao.dias_pos_ultimo_parto = calcula_dias_pos_ultimo_parto(bovino)
        gestacao.del_dias_em_lactacao = calcula_del(bovino)
        gestacao.ipc = calcula_ipc(bovino, inseminacao.data_inseminacao)

        return gestacao.save()

    def get_success_url(self, **kwargs):
        return reverse_lazy('visualizar_inseminacao', args=(self.inseminacao.id,))

class ConfirmarInseminacaUpdateView2(UpdateView):
    template_name = 'app_bovino/editar/diagnostico_gestacao.html'
    model = Gestacao
    form_class = FormGestacaoDiagnostico
    inseminacao = None

    def form_valid(self, form):
        self.inseminacao = Inseminacao.objects.get(id=self.object.inseminacao.id)
        self.inseminacao.status_inseminacao = True
        self.inseminacao.save()

        self.bovino = Bovino.objects.get(id=self.inseminacao.vaca.id)
        self.bovino.status_reprodutivo = StatusReprodutivo.objects.filter(status_reprodutivo=STATUS_REPRODUTIVO_PRENHA)
        self.bovino.save()

        self.object = form.save(commit=False)
        self.object.save()
        return redirect(self.get_success_url())

    def get_success_url(self, **kwargs):
        return reverse_lazy('visualizar_inseminacao', args=(self.inseminacao.id,))

def visualizarInseminacao(request, id):
    inseminacao = Inseminacao.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/inseminacao.html', {'inseminacao': inseminacao})


def excluirInseminacao(request, id):
    inseminacao = Inseminacao.objects.get(id=id)
    gestacao = Gestacao.objects.get(inseminacao=id)

    if request.method == 'POST':
        gestacao.delete()
        inseminacao.delete()
        return listarInseminacao(request)

    return render(request, 'app_bovino/excluir/inseminacao.html', {'inseminacao': inseminacao})

def listarInseminacao(request):
    objects = Inseminacao.objects.all().order_by('-data_inseminacao')
    return render(request, 'app_bovino/listar/inseminacao.html', {'objects': objects})


def gestacao_cadastrar(inseminacao, bovino):
    data_prevista_diagnostico_gestacao = calcula_data_prevista_diagnostico_gestacao(inseminacao.data_inseminacao)
    data_prevista_parto = calcula_data_prevista_parto(inseminacao.data_inseminacao)
    data_prevista_secar = calcula_data_prevista_secar(inseminacao.data_inseminacao)
    data_ultimo_parto = get_data_ultimo_parto(bovino)
    dias_pos_ultimo_parto = calcula_dias_pos_ultimo_parto(bovino)
    del_dias_em_lactacao = calcula_del(bovino)
    ipc = calcula_ipc(bovino, inseminacao.data_inseminacao)

    gestacao = Gestacao(
        inseminacao=inseminacao,
        data_prevista_diagnostico_gestacao=data_prevista_diagnostico_gestacao,
        data_prevista_parto=data_prevista_parto,
        data_prevista_secar=data_prevista_secar,
        data_ultimo_parto=data_ultimo_parto,
        dias_pos_ultimo_parto=dias_pos_ultimo_parto,
        del_dias_em_lactacao=del_dias_em_lactacao,
        ipc=ipc
    )

    return gestacao.save()