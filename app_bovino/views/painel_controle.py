#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect

#@login_required
def painelControle(request):
    data = []
    keys = ['model', 'url_cadastrar', 'url_listar', 'url_visualizar', 'url_editar']
    model = ['Bovino', 'Descarte de Bovinos', 'Banco de Espermas', 'Inseminação', 'Gestações', 'Status Reprodutivo', 'Lote', 'Categoria', 'Movimentação dos Bovinos']
    url_cadastrar = ['cadastrar_bovino', 'cadastrar_descarte', 'cadastrar_esperma', 'cadastrar_inseminacao', 'cadastrar_gestacao', 'cadastrar_status_reprodutivo_bovino', 'cadastrar_lote_bovino', 'cadastrar_categoria_bovino', '']
    url_listar = ['listar_bovino','listar_descarte', 'listar_esperma', 'listar_inseminacao', 'listar_gestacao', 'listar_status_reprodutivo_bovinos', 'listar_lote_bovinos', 'listar_categoria_bovinos', 'listar_movimentacao_bovinos']
    url_visualizar = ['visualizar_bovino', 'visualizar_descarte', 'visualizar_esperma', 'visualizar_inseminacao', 'visualizar_gestacao', 'visualizar_status_reprodutivo_bovino', 'visualizar_lote_bovino', 'visualizar_categoria_bovino', '']
    url_editar = ['editar_bovino', 'editar_descarte', 'editar_esperma', 'editar_inseminacao', 'editar_gestacao', 'editar_status_reprodutivo_bovino', 'editar_lote_bovino', 'editar_categoria_bovino', '']

    tamanho = len(model)
    for i in range(tamanho):
        temp = [model[i], url_cadastrar[i], url_listar[i], url_visualizar[i], url_editar[i]]
        data.append(dict(zip(keys, temp)))
    return render(request, 'app_bovino/painel_controle.html', {'cards': data})
