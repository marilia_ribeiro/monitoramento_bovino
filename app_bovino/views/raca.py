#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from app_bovino.forms import FormRaca
from app_bovino.models import Raca

#@login_required
def cadastrarRaca(request):
    # um HTTP post?
    if request.method == 'POST':
        form = FormRaca(request.POST)

        # forms é valido
        if form.is_valid():
            raca = form.save(commit=True)
            return HttpResponseRedirect(reverse(visualizarRaca, args=(raca.id,)))
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = FormRaca()

    return render(request, 'app_bovino/cadastrar/raca.html', {'form': form})

def visualizarRaca(request, id):
    raca = Raca.objects.get(id=id)
    return render(request, 'app_bovino/visualizar/raca.html', {'raca': raca})

def editarRaca(request, id):
    if request.method == 'POST':
        if id:
            raca = Raca.objects.get(id=id)
            form = FormRaca(request.POST,instance=raca)
        else:
            form = FormRaca(request.POST)
        if form.is_valid():
            raca = form.save()
            return HttpResponseRedirect(reverse(visualizarRaca, args=(raca.id,)))
    else:
        if id:
            raca = Raca.objects.get(id=id)
            form = FormRaca(instance=raca)

    return render(request, 'app_bovino/editar/raca.html', {'form':form, 'raca': raca})

def excluirRaca(request, id):
    raca = Raca.objects.get(id=id)

    if request.method == 'POST':
        raca.delete()
        return listarRaca(request)

    return render(request, 'app_bovino/excluir/raca.html', {'raca': raca})

def listarRaca(request):
    racas = Raca.objects.all()
    return render(request, 'app_bovino/listar/raca.html', {'racas': racas})
